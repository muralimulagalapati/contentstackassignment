'use strict'

const sessionRouter = require('express').Router()
const sessionModel = require('../models/session')

const createSession = async (req, res) => {
  const session = new sessionModel({
    active: req.body.userid
  })

  try {
    await session.save()
    res.sendStatus(201)
  } catch(ex) { 
    console.error(ex)
    res.sendStatus(500)
  }
}

const destroySession = async (req, res) => {
  const record = {
    active: req.body.userid
  }

  try {
    await sessionModel.findOneAndRemove(record).exec()
    res.sendStatus(200)
  } catch(ex) { 
    console.error(ex)
    res.sendStatus(500)
  }
}

sessionRouter.post('/', createSession)
sessionRouter.delete('/', destroySession)
module.exports = sessionRouter
