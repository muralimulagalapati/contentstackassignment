'use strict'

const tweetsRouter = require('express').Router()
const tweetsModel = require('../models/tweets')
const sessionModel = require('../models/session')

const getTweets = async (req, res) => {
  let { projection, limit = 1, lastid = null } = req.query

  limit = parseInt(limit, 10)

  projection = projection && projection.split(',')
  const projectionObj = {}
  if (projection) {
    projection.forEach(proj => (
      proj && (projectionObj[proj] = 1)
    ))
  }
  const paginate = lastid ? {'_id': {'$gt': lastid}} : {}
  let populate = (Object.keys(projectionObj).length && 
    !projectionObj.hasOwnProperty('author')) ? '' : 'author'

  try {
    const tweets = (
      await tweetsModel
      .find(paginate, projectionObj)
      .populate(populate)
      .limit(limit).exec()
    )
    const reponseData = tweets && Object.keys(tweets).length
      ? { tweets, lastid: tweets[tweets.length-1]['_id'] } : {}
    res.json(reponseData)
  } catch (ex) {
    console.error(ex)
    res.sendStatus(404)
  }
}

const postTweet = async (req, res) => {
  try {
    const tweet = new tweetsModel(req.body)
    await tweet.save()
    res.sendStatus(201)
  } catch (ex) {
    console.error(ex)
    res.sendStatus(404)
  }
}

const updateTweet = async (req, res) => {
  const { tweetid } = req.params
  try {
    await tweetsModel.findByIdAndUpdate(tweetid, req.body).exec()
    res.sendStatus(200)
  } catch (ex) {
    console.error(ex)
  }
}

const deleteTweet = async (req, res) => {
  const { tweetid } = req.params
  try {
    await tweetsModel.findByIdAndRemove(tweetid).exec()
    res.sendStatus(200)
  } catch (ex) {
    console.error(ex)
  }
}

tweetsRouter.post('/', postTweet)
tweetsRouter.get('/', getTweets)
tweetsRouter.put('/:tweetid', updateTweet)
tweetsRouter.delete('/:tweetid', deleteTweet)
module.exports = tweetsRouter
