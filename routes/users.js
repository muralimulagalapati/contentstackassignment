'use strict'

const usersRouter = require('express').Router()
const usersModel = require('../models/users')
const tweetsModel = require('../models/tweets')
const sessionModel = require('../models/session')

const createUser = async (req, res) => {
  const user = new usersModel(req.body)
  try {
    await user.save()
    res.sendStatus(201)
  } catch (ex) {
    console.error(ex)
    res.sendStatus(500)
  }
}

const getUsers = async (req, res) => {
  try {
    const users = await usersModel.find().exec()
    res.status(200).json(users)
  } catch (ex) {
    console.error(ex)
    res.sendStatus(404)
  }
}

const updateUser = async (req, res) => {
  const { userid } = req.params
  try {
    await usersModel.findByIdAndUpdate(userid, req.body).exec()
    res.sendStatus(200)
  } catch (ex) {
    console.error(ex)
    res.sendStatus(404)
  }
}

const getFeed = async (req, res) => {
  const query = [{
    $lookup: {
      from: 'sessions',
      localField: 'author',
      foreignField: 'active',
      as: 'item'
    }
  }]

  try {
    let users = await tweetsModel.aggregate(query).exec()
    users = users.filter(user => user.item && user.item.length)
    res.json(users)
  } catch (ex) {
    console.error(ex)
    res.sendStatus(404)
  }
}

usersRouter.post('/', createUser)
usersRouter.get('/', getUsers)
usersRouter.put('/:userid', updateUser)
usersRouter.get('/feeds', getFeed)
module.exports = usersRouter
