'use strict'

const mongoose = require('mongoose')

const options = { timestamps: true }

const TweetsSchema = new mongoose.Schema({
  tweet: { type: String, required: true, max: 280 },
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true }
}, options)

TweetsSchema
.virtual('url')
.get(function () {
  return `/tweets/${this._id}`
})

module.exports = mongoose.model('Tweets', TweetsSchema)
