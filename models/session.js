'use strict'

const mongoose = require('mongoose')

const SessionSchema = new mongoose.Schema({
  active: { type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true }
})

module.exports = mongoose.model('Session', SessionSchema)
