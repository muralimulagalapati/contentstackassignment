'use strict'

const mongoose = require('mongoose')

const options = { timestamps: true }

const UsersSchema = new mongoose.Schema({
  first_name: { type: String, required: true, max: 100 },
  last_name: { type: String, required: true, max: 100 },
  username: { type: String, required: true, max: 50 }
}, options)

UsersSchema
.virtual('name')
.get(function () {
  return `${this.first_name}, ${this.last_name}`
})

UsersSchema
.virtual('url')
.get(function () {
  return `/users/${this._id}`
})

module.exports = mongoose.model('Users', UsersSchema)
