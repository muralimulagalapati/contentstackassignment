'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const mongoose = require('mongoose')

const tweetsRouter = require('./routes/tweets')
const usersRouter = require('./routes/users')
const sessionRouter = require('./routes/session')

const mongoUrl = 'mongodb://murali:murali1@ds145562.mlab.com:45562/contentstack'
const app = express()
const { PORT = 3000 } = process.env

// Initialize Mongo
mongoose.connect(mongoUrl, { useNewUrlParser: true })
mongoose.Promise = global.Promise
const db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error'))

// middleware
app.use(bodyParser.json({ strict: false }))
app.use(morgan('combined'))

app.use('/tweets', tweetsRouter)
app.use('/users', usersRouter)
app.use('/session', sessionRouter)
app.get('/ping', (_, res) => res.send('pong'))

const server = app.listen(PORT, () => {
  console.info(`Server is listening on ${PORT}`)
})

server.on('error', console.error.bind(console, 'Error starting server'))
